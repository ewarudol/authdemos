# AuthDemos
## Info
- ASP.NET Core 3 authentication and authorization demos
- This repository is under development only for educational purposes, it can contain raw code, too many comments and mistakes/errors
- Ideas, implementation and knowledge from https://invidious.snopyta.org/watch?v=Fhfvbl_KbWo

## Content (baseAuth)
- Adding authentication and authorization middleware
- Injecting authorization policies
- Injecting cookies based authentication
- Injecting identity based authentication (with code-first users database)
- Creating custom requirements for policy builder
- Study of UserManager and SignInManager - user creation, registration, login and logout
- Use of [Authorize] attribute
- Study of Claims, Roles, Policies 
- Global app filter and [AllowAnonymous]
- Manual use of AuthorizationService

## Content (oAuth)
- Creating JWT Token with symmetric key based signature 
- Adding "in Header" or "in Query" Bearer JWT Authorization
- Full implementation of a OAuth flow (Client, Authorization Server, Resources Server) with explainations (see memo pictures)
- Handling expiration of a token
- Sample of token refreshing with rewriting HttpContext tokens
- Resource securing - policy based on Authorization Server requests 

![oauth1](oauth1.png)
![oauth2](oauth2.png)
![oauth3](oauth3.png)