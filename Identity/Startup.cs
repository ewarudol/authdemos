using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Identity.Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Identity
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            //*** Inject PoliceDatabase to application
            services.AddDbContext<PoliceDbContext>(config =>
            {
                config.UseInMemoryDatabase("PoliceDatabase");
            });
            
            //*** Inject Identity Helpers to application (which works only with specified generic types for role and user)
            //*** Attention! Identity by default adds cookie authentication! You need to ConfigureApplicationCookie later
            services.AddIdentity<IdentityUser, IdentityRole>(config =>
                {
                    config.Password.RequiredLength = 4;
                    config.Password.RequireDigit = false;
                    config.Password.RequireNonAlphanumeric = false;
                    config.Password.RequireUppercase = false;
                })
                .AddEntityFrameworkStores<PoliceDbContext>() //*** Tell Identity service where to look for identity?
                .AddDefaultTokenProviders(); //*** Add default way of generating tokens in app (like token to reset password etc)

            services.ConfigureApplicationCookie(config =>
            {
                config.Cookie.Name = "Policeman.Cookie"; //*** You need Policeman cookie to get in ***
                config.LoginPath = "/Home/Login"; //*** If you have no Policeman cookie, you will be redirected here to get one ***
            });
            
            //*** Register Authorization Handler to be injected in Authorization Middleware ***
            //*** How does it work?
            //*** Authorization Middleware will get all policies with its requirements and will be trying to get handler for each requirement 
            services.AddAuthorization(config =>
            {
                //*** building your own shiny policy from the given bricks (requirements)
                var policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .RequireClaim("Occupation", "policeOfficer")
                    .Build();
                config.AddPolicy("OnlyOfficersPolicy", policy);
            });

            services.AddControllersWithViews();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
 
            //*** Add Authentication Middleware (who are you?) ***
            app.UseAuthentication();
            
            //*** Add Authorization Middleware (are you allowed?) ***
            app.UseAuthorization();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });
        }
    }
}