using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Identity.Database
{
    public class PoliceDbContext : IdentityDbContext //*** IdentityDbContext contains User table! Check it out
    {
        public PoliceDbContext(DbContextOptions<PoliceDbContext> options) : base(options)
        {
            
        }
    }
}