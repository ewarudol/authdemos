using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Identity.Database;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Identity.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;
        private PoliceDbContext _dbContext;

        //*** UserManager - service for creating users, looking for them etc
        //*** SignInManager - service for dealing with Http to sign in/out
        public HomeController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager, PoliceDbContext dbContext)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _dbContext = dbContext; // DONT DO THAT, LIKE NEVER
        }
        
        public IActionResult Index()
        {
            return View();
        }
        
        public IActionResult Login()
        {
            return View();
        }
        
        public IActionResult Register()
        {
            return View();
        }
        
        [Authorize(Policy = "OnlyOfficersPolicy")] //*** Check in Authorization Middleware if I can go in (make sure there is such a middleware!) ***
        public IActionResult Secret()
        {
            string users = JsonConvert.SerializeObject(_dbContext.Users); // DONT DO THAT!!!
            string claims = JsonConvert.SerializeObject(_dbContext.UserClaims);
            return View("Secret", users+claims);
        }
        
        [HttpPost]
        public async Task<IActionResult> Login(string userName, string password)
        {
            var user = await _userManager.FindByNameAsync(userName);

            if (user != default)
            {
                //*** isPersistant = false (cookie will disappear after closing a browser)
                //*** lockoutOnFailure = false (user won't be blocked in db after failure)
                await _signInManager.PasswordSignInAsync(user, password, false, false); //*** Creates user's cookie
            }

            return RedirectToAction("Index"); 
        }
        
        [HttpPost]
        public async Task<IActionResult> Register(string userName, string password)
        {
            var user = new IdentityUser
            {
                UserName = userName
            };
            
            var res = await _userManager.CreateAsync(user, password); //*** Create user in the db

            if (!res.Succeeded)
                return Content(JsonConvert.SerializeObject(res.Errors));
            
            if (userName.Contains("officer")) //*** Add Claim (IMPORTANT - after user creation!)
            {
                await _userManager.AddClaimAsync(user, new Claim("Occupation", "policeOfficer"));
            }
            return RedirectToAction("Index"); 
        }

        public async Task<IActionResult> LogOut()
        {
            await _signInManager.SignOutAsync(); //*** Delete user's cookie
            return RedirectToAction("Index");
        }
    }
}