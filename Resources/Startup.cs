using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Resources
{
    // The client accesses protected resources by presenting the access
    // token to the resource server.  The resource server MUST validate the
    // access token and ensure that it has not expired and that its scope
    // covers the requested resource.  The methods used by the resource
    // server to validate the access token (as well as any error responses)
    // are beyond the scope of this specification but generally involve an
    // interaction or coordination between the resource server and the
    // authorization server.
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            //*** Warning! We need to add at least dummy authentication scheme because it is in .NET Core fallback for authorization failures
            services.AddAuthentication(options =>
            {
                options.DefaultChallengeScheme = "AlwaysForbidScheme";
                // you can also skip this to make the challenge scheme handle the forbid as well
                options.DefaultForbidScheme = "AlwaysForbidScheme";
                options.AddScheme<AlwaysForbidSchemeHandler>("AlwaysForbidScheme", "Always Forbid Scheme");
            });

            services.AddHttpClient();
            services.AddHttpContextAccessor();

            services.AddAuthorization(config =>
            {
                //*** In Resources project to get to secured endpoint you need a token in thea header, token after that is validated in Authorization Server 
                var defaultAuthBuilder = new AuthorizationPolicyBuilder();
                var defaultAuthPolicy = defaultAuthBuilder
                    .AddRequirements(new JwtRequirement())
                    .Build();
                
                config.DefaultPolicy = defaultAuthPolicy;
            });
            
            services.AddScoped<IAuthorizationHandler, JwtRequirementHandler>();
            
            services.AddControllers();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });
        }
    }
}