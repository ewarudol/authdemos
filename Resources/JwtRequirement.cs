using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Infrastructure;

namespace Resources
{
    public class JwtRequirement : IAuthorizationRequirement
    {
        
    }

    public class JwtRequirementHandler : AuthorizationHandler<JwtRequirement>
    {
        private HttpClient _client;
        private HttpContext _httpContext;


        public JwtRequirementHandler(IHttpClientFactory clientFactory, IHttpContextAccessor accessor)
        {
            _client = clientFactory.CreateClient();
            _httpContext = accessor.HttpContext;
        }
        
        protected override async Task HandleRequirementAsync(
            AuthorizationHandlerContext context, 
            JwtRequirement requirement)
        {
            if (_httpContext.Request.Headers.TryGetValue("Authorization", out var authHeader))
            {
                var access_token = "";
                try
                {
                    access_token = authHeader.ToString().Split(' ')[1];
                }
                catch
                {
                    context.Fail();
                }
                
                var response = await _client.GetAsync($"http://localhost:7000/oauth/validate?access_token={access_token}");

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    context.Succeed(requirement);
                }
            }
        }
    }
}