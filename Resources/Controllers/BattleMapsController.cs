using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Resources.Controllers
{
    public class BattleMapsController : Controller
    {
        [Authorize]
        public string Index()
        {
            return "[Secret Strategic Naboo Map] Enemy is in the woods.";
        }
    }
}