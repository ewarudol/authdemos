using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;

namespace Client
{
    
    //+----------+
    //| Resource |
    //|   Owner  |
    //|          |
    //+----------+
    //     ^
    //     |
    //    (B)
    //+----|-----+          Client Identifier      +---------------+
    //|         -+----(A)-- & Redirection URI ---->|               |
    //|  User-   |                                 | Authorization |
    //|  Agent  -+----(B)-- User authenticates --->|     Server    |
    //| (browser)|                                 |               |
    //|         -+----(C)-- Authorization Code ---<|               |
    //+-|----|---+                                 +---------------+
    //  |    |                                         ^      v
    // (A)  (C)                                        |      |
    //  |    |                                         |      |
    //  ^    v                                         |      |
    //+---------+                                      |      |
    //|         |>---(D)-- Authorization Code ---------'      |
    //|  Client |          & Redirection URI                  |
    //|   App   |                                             |
    //|         |<---(E)----- Access Token -------------------'
    //+---------+       (w/ Optional Refresh Token)
    
    //Redirection URI (Callback URL) - tells AuthServer where to get me back with authorization code
    
    // Flow chronology:
    // A - ClientApp sends welcome page to the browser
    // A - In the browser user clicks "Authenticate" or "Secret Database" button (and sends necessary data using ClientId and CallbackPath from Startup config)
    // B - (Authorize GET-endpoint in the server) Authorization Server sends authorize form to the browser
    // B - User fills in form and sends it (Authorize POST-endpoint in the server), Server checks if he knows user
    // C - Authorization Server brings user to the requested callback url with code associated with user
    // D - WARNING! Middleware MAGICALLY intercepts CallbackPath request (/oauth/callback) and performs config.TokenEndpoint (from Startup config) request 
    // D - And finally - you know you've used [Authorize] attribute to perform magic-auth-dance to get into secured page in client app, middleware
    // will produce cookie for you and get you there
    
    //and remember in that particular case client = resource owner

    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            //*** Register Authentication Handler to be injected in Authorization Middleware ***
            //*** OAuth FLOW - go to OurServer to get cookie 
            services.AddAuthentication(config =>
                {
                    // *** Cookies are the way to Authenticate user in Client App
                    config.DefaultAuthenticateScheme = "ClientCookie";
                    // *** signing in results in getting cookie
                    config.DefaultSignInScheme = "ClientCookie";
                    // *** Most important! To check if we are allowed, go and ask OurServer
                    config.DefaultChallengeScheme = "OurServer";
                })
                .AddCookie("ClientCookie")
                .AddOAuth("OurServer", config =>
                {
                    config.ClientId = "StandardTrooperDashboard-23"; //*** or for example identifier of your Google account
                    config.ClientSecret = "BUISAFUIPHFPUIWFHPIWQ";
                    config.CallbackPath = "/oauth/callback"; //*** where to get me back with authorization code
                    config.AuthorizationEndpoint = "http://localhost:7000/oauth/authorize"; //*** where is Authorization Server
                    config.TokenEndpoint = "http://localhost:7000/oauth/token"; //*** where to exchange code for a token
                    config.SaveTokens = true; //after getting external authorization done and producing cookie don't destroy token
                    
                    //subscribe to creating cookie (ticket) event so we can extract access token an meld it into identity (and retrieve external claims from server)  
                    config.Events = new OAuthEvents()
                    {
                        OnCreatingTicket = context =>
                        {
                            var access_token = context.AccessToken;
                            var base64Payload = access_token.Split('.')[1];
                            var bytes = Convert.FromBase64String(base64Payload);
                            var jsonPayload = Encoding.UTF8.GetString(bytes);
                            var claims = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonPayload);
                            foreach (var claim in claims)
                            {
                             context.Identity.AddClaim(new Claim(claim.Key, claim.Value));   
                            }
                            return Task.CompletedTask;
                        }
                    };
                });
            
            services.AddHttpClient();
            services.AddControllersWithViews();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            
            //*** Add Authentication Middleware (who are you?) ***
            app.UseAuthentication();
            
            //*** Add Authorization Middleware (are you allowed?) ***
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });
        }
    }
}