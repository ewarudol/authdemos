using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Client.Controllers
{
    public class HomeController : Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public HomeController(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }
        
        public async Task<IActionResult> Index()
        {
            var token = await HttpContext.GetTokenAsync("access_token");
            var userinfo = HttpContext?.User?.Claims?.Where(x => x.Type == "sub")?.FirstOrDefault()?.Value;
            return View("Index", userinfo);
        }
        
        [Authorize] 
        public async Task<IActionResult> Authenticate()
        {
            var token = await HttpContext.GetTokenAsync("access_token");
            var userinfo = HttpContext.User.Claims.Where(x => x.Type == "sub").FirstOrDefault().Value;
            return View("Index", userinfo);
        }

        [Authorize] 
        public async Task<IActionResult> Secret()
        {
            var token = await HttpContext.GetTokenAsync("access_token");
            var userinfo = HttpContext.User.Claims.Where(x => x.Type == "sub").FirstOrDefault().Value;
            return View("Secret", userinfo);
        }

        public async Task<IActionResult> GetBattleMap()
        {
            var token = await HttpContext.GetTokenAsync("access_token");
            var refresh_token = await HttpContext.GetTokenAsync("refresh_token");
            
            var client = _httpClientFactory.CreateClient();
            client.DefaultRequestHeaders.Add("Authorization", $"Bearer {token}");
            var response = await client.GetAsync("http://localhost:9000/BattleMaps/index");

            await RefreshAccessToken();

            var token2 = await HttpContext.GetTokenAsync("access_token");
            var refresh_token2 = await HttpContext.GetTokenAsync("refresh_token");
             
            var client2 = _httpClientFactory.CreateClient();
            client2.DefaultRequestHeaders.Add("Authorization", $"Bearer {token2}");
            var response2 = await client2.GetAsync("http://localhost:9000/BattleMaps/index");

            if (response2.IsSuccessStatusCode)
            {
                var txt = await response2.Content.ReadAsStringAsync();
                return Ok(txt);
            }

            return Ok($"Maps server says: {response2.StatusCode}");
        }

        
        //Request according to:  https://tools.ietf.org/html/rfc6749#section-6
        //POST /token HTTP/1.1
        //Host: server.example.com
        //    Authorization: Basic czZCaGRSa3F0MzpnWDFmQmF0M2JW
        //Content-Type: application/x-www-form-urlencoded
        public async Task<string> RefreshAccessToken()
        {
            var refresh_token = await HttpContext.GetTokenAsync("refresh_token");
            var client = _httpClientFactory.CreateClient();
            
            var requestData = new Dictionary<string, string>
            {
                ["grant_type"] = "refresh_token",
                ["refresh_token"] = refresh_token
            };
            var request = new HttpRequestMessage(HttpMethod.Post, "http://localhost:7000/oauth/token")
            {
                Content = new FormUrlEncodedContent(requestData)
            };

            var basicCredentials = "username:password";
            var encodedCredantials = Encoding.UTF8.GetBytes(basicCredentials);
            var base64Credentials = Convert.ToBase64String(encodedCredantials);
            
            request.Headers.Add("Authorization", $"Basic {base64Credentials}");

            var response = await client.SendAsync(request);

            if (response.StatusCode == HttpStatusCode.Unauthorized)
                throw new Exception("You have nothing to refresh dumbass!!!!!!!");
            
            var responseString = await response.Content.ReadAsStringAsync();
            var responseData = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseString);

            var newAccessToken = responseData.GetValueOrDefault("access_token");
            var newRefreshToken = responseData.GetValueOrDefault("refresh_token");

            //rewrite current tokens
            var authInfo = await HttpContext.AuthenticateAsync("ClientCookie");
            authInfo.Properties.UpdateTokenValue("access_token", newAccessToken);
            authInfo.Properties.UpdateTokenValue("refresh_token", newRefreshToken);

            await HttpContext.SignInAsync("ClientCookie", authInfo.Principal, authInfo.Properties);
            
            return "";
        }
        
    }
}