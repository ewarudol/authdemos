using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.IdentityModel.Tokens;
using JwtRegisteredClaimNames = Microsoft.IdentityModel.JsonWebTokens.JwtRegisteredClaimNames;

namespace ServerBearerToken.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        public IActionResult Secret()
        {
            return View();
        }

        public IActionResult Authenticate()
        {
            // *** JWT CLAIMS
            // http://tools.ietf.org/html/rfc7519#section-4
            // None of the claims [of JWT documentation] are intended to be mandatory to use or implement in all
            // cases. Applications using JWTs should define which specific claims they use and when they are required or optional.

            // *** User Claims
            var claim = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, "Ewa"), //*** Sub => ID
                new Claim("Miejsce", "Wilda")
            };

            // *** JWT STRUCTURE:
            // - Header (algorithm),
            // - Payload (claims, nbf, exp etc),
            // - Signature (Header + Payload encrypted with algo and internal secret) 
            // If you want to validate token you need to create signature by yourself
            // and compare with that given by the token
            
            // *** Creating signature
            var secretBytes = Encoding.UTF8.GetBytes("zaq1@WSXzaq1@WSXzaq1@WSX");
            var key = new SymmetricSecurityKey(secretBytes);
            var algo = SecurityAlgorithms.HmacSha256; //*** Popular algo
            var signingCredentials = new SigningCredentials(key, algo);
            
            // *** Creating token
            var token = new JwtSecurityToken(
                "http://localhost:2000",  //*** Who will be requesting for an authorization
                "http://localhost:2000",  //*** Resource Servers that should accept the token
                claim, //*** issuer claims
                notBefore: DateTime.Now,  //*** notbefore - when token starts to be valid
                expires: DateTime.Now.AddMinutes(1), //*** expires - when token end to be valid
                signingCredentials
                );

            var tokenJson = new JwtSecurityTokenHandler().WriteToken(token);
            
            return Content("To access restricted area copy token and place it in a header (as Authorization Bearer) or in a query (as access_token): " + tokenJson);
        }
    }
}