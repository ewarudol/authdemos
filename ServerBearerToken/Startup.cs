using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.VisualBasic;

namespace ServerBearerToken
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthorization(config =>
            {
                //*** building your own shiny policy from the given bricks (requirements)
                var policyBuilder = new AuthorizationPolicyBuilder();
                var policy = policyBuilder
                    .RequireAuthenticatedUser()
                    .Build();
            });
            
            //*** Register Authentication Handler to be injected in Authorization Middleware ***
             services.AddAuthentication("OAuth")
                 //*** Add Bearer - so you will need to have in the request header pair: <"Authorization", "Bearer tokenContent">
                 .AddJwtBearer("OAuth", config => 
                 {
                     var secretBytes = Encoding.UTF8.GetBytes("zaq1@WSXzaq1@WSXzaq1@WSX");
                     var key = new SymmetricSecurityKey(secretBytes);
                     
                     config.TokenValidationParameters = new TokenValidationParameters()
                     {
                         ValidIssuer = "http://localhost:2000", //*** Who will be requesting for an authorization
                         ValidAudience = "http://localhost:2000", //*** Resource Servers that should accept the token
                         IssuerSigningKey = key, //*** We tell server how to reconstruct signature from Header+Payload
                         
                         RequireExpirationTime = true,
                         ValidateLifetime = true,
                         LifetimeValidator = (DateTime? notBefore, DateTime? expires, SecurityToken securityToken, 
                             TokenValidationParameters validationParameters) => 
                         {
                             return notBefore <= DateTime.UtcNow &&
                                    expires >= DateTime.UtcNow;
                         }
                     };
                     
                     //*** You can now pass token by query by ?access_token=tokenContent
                     config.Events = new JwtBearerEvents()
                     {
                         OnMessageReceived = context =>
                         {
                             var queryTokenKey = "access_token";
                             if (context.Request.Query.ContainsKey(queryTokenKey))
                             {
                                 context.Token = context.Request.Query[queryTokenKey];
                             }
                             return Task.CompletedTask;
                         }
                     };
                     
                 });

             services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            
            //*** Add Authentication Middleware (who are you?) ***
            app.UseAuthentication();
            
            //*** Add Authorization Middleware (are you allowed?) ***
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });
        }
    }
}