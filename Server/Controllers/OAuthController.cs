using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace Server.Controllers
{
    public class OAuthController : Controller
    {
        //*** Client will come here for authorization routine, so we will display a from for a username (simplified) and we
        // will handle that in the POST method beneath 
        [HttpGet]
        public IActionResult Authorize(
            string response_type, //authorization flow type
            string client_id, 
            string redirect_uri,
            string scope, //what info I want from server
            string state //random string to confirm we are going back to the same client
            )
        {
            // *** we want to pass that data to the view so we will forward this to method beneath with user credentials (username)
            var query = new QueryBuilder(); 
            query.Add("redirect_uri", redirect_uri); //http://localhost:8000/oauth/callback
            query.Add("state", state);
            
            return View("Authorize", query.ToString());
        }

        // *** We will hit that endpoint from uur Authorization View
        [HttpPost]
        public IActionResult Authorize(
            string userName, 
            string redirect_uri,
            string state)
        {
            if (!userName.Contains("CT"))
                return StatusCode(401);
            
            string code = "i_know_you_here_is_code_to_be_exchanged_for_token";
            
            var query = new QueryBuilder(); 
            query.Add("code", code);
            query.Add("state", state);
            
            return Redirect($"{redirect_uri}{query.ToString()}"); //http://localhost:8000/oauth/callback
        }

        public async Task<IActionResult> Token(
            string grant_type,
            string code, // code given to us after successful authorization to be exchanged for a token
            string redirect_uri, 
            string client_id,
            string refresh_token
            )
        {
            //validate code (for example with that what is stored in db)
            //SOME AWESOME CODE 
            if(code != "i_know_you_here_is_code_to_be_exchanged_for_token" && string.IsNullOrEmpty(refresh_token))
                return StatusCode(401);
            
            // *** JWT CLAIMS
            // http://tools.ietf.org/html/rfc7519#section-4
            // None of the claims [of JWT documentation] are intended to be mandatory to use or implement in all
            // cases. Applications using JWTs should define which specific claims they use and when they are required or optional.

            // *** User Claims
            var claim = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, "Person - Clone Trooper"), //*** Sub => ID
                new Claim("AssignedJedi", "Skywalker")
            };

            // *** JWT STRUCTURE:
            // - Header (algorithm),
            // - Payload (claims, nbf, exp etc),
            // - Signature (Header + Payload encrypted with algo and internal secret) 
            // If you want to validate token you need to create signature by yourself
            // and compare with that given by the token
            
            // *** Creating signature
            var secretBytes = Encoding.UTF8.GetBytes(Constants.Secret);
            var key = new SymmetricSecurityKey(secretBytes);
            var algo = SecurityAlgorithms.HmacSha256; //*** Popular algo
            var signingCredentials = new SigningCredentials(key, algo);
            
            // *** Creating token
            var token = new JwtSecurityToken(
                Constants.Issuer,  //*** Who will be requesting for an authorization
                Constants.Audience,  //*** Resource Servers that should accept the token
                claim, //*** issuer claims
                notBefore: DateTime.Now,  //*** notbefore - when token starts to be valid
                expires: grant_type == "refresh_token" ? DateTime.Now.AddMinutes(5): DateTime.Now, //creation of useless (without refreshing) token - exp time now
                //*** expires - when token end to be valid, if we PROPERLY VALIDATE INCOMING REFRESH TOKEN with database xd then we can postpone expiration
                signingCredentials
            );

            var access_token = new JwtSecurityTokenHandler().WriteToken(token);

            var responseObject = new
            {
                access_token,
                token_type = "Bearer",
                raw_claim = "oauthTutorial",
                refresh_token = "YourSuperRefreshToken"
            };
            
            return Json(responseObject);
        }
        
        [Authorize]
        public IActionResult Validate()
        {
            //Very sophisticated validation mechanism goes here
            if (HttpContext.Request.Query.TryGetValue("access_token", out var access_token))
            {
                return Ok();
            }
            
            return BadRequest();
        }
    }
}