using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace Basics
{
    public class CustomRequirement : IAuthorizationRequirement //*** Cooking own requirement
    {
        public string ClaimType { get; }
        
        public CustomRequirement(string claimType)
        {
            ClaimType = claimType;
        }
    }

    public class CustomRequirementHandler : AuthorizationHandler<CustomRequirement> //*** All requirements need handler
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, CustomRequirement requirement)
        {
            var hasClaim = context.User.Claims.Any(x => x.Type == requirement.ClaimType);
            if (hasClaim)
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }

    public static class AuthorizationPolicyBuilderExtensions
    {
        public static AuthorizationPolicyBuilder RequireCustomClaim(this AuthorizationPolicyBuilder builder, string claimType)
        {
            builder.AddRequirements(new CustomRequirement(claimType));
            return builder;
        }
    }
}