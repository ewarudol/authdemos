using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Basics.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        
        public IActionResult Login()
        {
            return View();
        }

        [Authorize(Policy = "Claim.DoB")] //*** Check in Authorization Middleware if I can go in (make sure there is such a middleware!) ***
        public IActionResult Secret()
        {
            return View();
        }
        
        [Authorize(Roles = "PoliceBoss")] //*** Check in Authorization Middleware if I can go in (make sure there is such a middleware!) ***
        public IActionResult SecretInfo()
        {
            return Content("You are the boss");
        }

        public IActionResult Authenticate()
        {
            var policeSecretaryClaims = new List<Claim>() //*** Police secretary will recognize you as an employee and give you her claims ***
            {
                new Claim(ClaimTypes.Surname, "Kowalsky" ), //*** Hello, you are Mr. Kowalsky
                new Claim("Secretary.Says", "I know you") //*** And you was recognized 
            };

            var magneticCardClaims = new List<Claim>()
            {
                new Claim(ClaimTypes.SerialNumber, "X5F4CE"), //*** Hello, your ID is CT5555
                new Claim(ClaimTypes.DateOfBirth, "11/11/2000"), //*** you DateOfBirth is 11/11/2000
                new Claim(ClaimTypes.Role, "PoliceBoss"), //*** You are the boss! Attention! Role in .NET is the same thing as claim and is concerned as legacy, so avoid it
                new Claim("MagneticCard.Says", "Allowed") //*** And you are allowed in 
            };
            
            //*** Your Identities on base of different institution claims
            var identityFromPoliceSecretary = new ClaimsIdentity(policeSecretaryClaims, "Police Secretary Identity"); 
            var identityFromMagneticCard = new ClaimsIdentity(magneticCardClaims, "Magnetic Card Identity"); 
            
            var userPrincipal = new ClaimsPrincipal(new[] {identityFromPoliceSecretary, identityFromMagneticCard} );

            //*** Encapsulate all above data into default auth schema (in that case Policeman.Cookie)
            HttpContext.SignInAsync(userPrincipal); //Sign in a principal for the default authentication scheme

            return RedirectToAction("Index");
        }
    }
}