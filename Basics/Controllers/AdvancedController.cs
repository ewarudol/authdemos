using System.ComponentModel;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Basics.Controllers
{
    public class AdvancedController : Controller
    {
        private IAuthorizationService _authorizationService;  //*** Call same mechanism as Middleware but manually 

        public AdvancedController(IAuthorizationService authorizationService)
        {
            _authorizationService = authorizationService;
        }

        public async Task<IActionResult> DoSthFragile()
        {
            var authRes = await _authorizationService.AuthorizeAsync(User, "Claim.SerialNumber");
            
            if(authRes.Succeeded)
                return Content("Fragile thing done");
            else
                return Content("You are not allowed, you have no serial number");
        }

        public async Task<IActionResult> DoSthFragile2([FromServices] IAuthorizationService service) //*** Crazy DI
        {
            var authRes = await service.AuthorizeAsync(User, "Claim.SerialNumber");
            
            if(authRes.Succeeded)
                return Content("Fragile thing done");
            else
                return Content("You are not allowed, you have no serial number");
        }
    }
}