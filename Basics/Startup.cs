using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Basics
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            //*** Register Authentication Handler to be injected in Authorization Middleware ***
            services.AddAuthentication("CookieAuth")
                .AddCookie("CookieAuth", config =>
                {
                    config.Cookie.Name = "Policeman.Cookie"; //*** You need Policeman cookie to get in ***
                    config.LoginPath = "/Home/Login"; //*** If you have no Policeman cookie, you will be redirected here to get one ***
                });

            //*** Register Authorization Handler to be injected in Authorization Middleware ***
            //*** How does it work?
            //*** Authorization Middleware will get all policies with its requirements and will be trying to get handler for each requirement 
            services.AddAuthorization(config =>
            {
                //*** building your own shiny policy from the given bricks (requirements)
                var policyBuilder = new AuthorizationPolicyBuilder();
                var kowalskyPolicy = policyBuilder
                    .RequireAuthenticatedUser()
                    .RequireClaim(ClaimTypes.Name, "Kowalsky")
                    .Build();
                config.AddPolicy("Claim.Surname", kowalskyPolicy);

                //*** building your own shiny policy from your custom bricks
                config.AddPolicy("Claim.DoB", policyBuilder =>
                {
                    policyBuilder.AddRequirements(new CustomRequirement(ClaimTypes.DateOfBirth));
                });
                
                //*** building your own shiny policy from your custom bricks (but with using extension method)
                config.AddPolicy("Claim.SerialNumber", policyBuilder =>
                {
                    policyBuilder.RequireCustomClaim(ClaimTypes.SerialNumber);
                });
            });

            //*** We added custom requirement so we need to inject handler! 
            services.AddScoped<IAuthorizationHandler, CustomRequirementHandler>();
            
            services.AddControllersWithViews(config =>
            {
                // *** Global filter - WHOLE SERVICE ONLY FOR AUTHORIZED USERS 
                // Filter can be broken with [AllowAnonymous] attribute (for example for logging in action) 
                var policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();
                //config.Filters.Add(new AuthorizeFilter(policy)); //temporary switched off
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
 
            //*** Add Authentication Middleware (who are you?) ***
            app.UseAuthentication();
            
            //*** Add Authorization Middleware (are you allowed?) ***
            app.UseAuthorization();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });
        }
    }
}